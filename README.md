## Welcome To Driver Scheduling application

# NodJs - Express - ReactJS - MYSQL- Unit Test - DOCKER -Swagger -OpenAPI

## Base Repo


Following things are ready to be used directly with the project.

- Auth(Sign up,Login,Role Permissions)
- User Manager
- Role Manager
- Permission Manager
- API Permission
- Seeders
- Drivers Schedules
- Unit test
- Open APIs
- Docker





## Packages Installed
- Nodejs  14.17.6
- npm 7.21.1
- nodemon 2.0.16


## Usage Server

- Clone/Download a repo 
- Run `cp .env.sample .env`
- Run `cd server`
- create new database with name  `dev`
- Run `npm i`
- Install sequelize by `npm i -g sequelize-cli`
- Run `sequelize db:migrate` for Migration 
- Run Seeder `sequelize db:seed:all` 
- Install nodemon by `npm install -g nodemon`
- Run `nodemon` 

## Usage Fronend

- Run `cd client`
- Run `cp .env.sample .env`
- Run `npm i`
- Run `npm start` 

## Users for Testing:
- Manager User : manager@test.com/manager@123
- Employee User: employee@test.com/employee@123

## Usage Karaoke

- Run `cd music`
- Run `npm i`
- Run `npm start`
## Musixmatch API
Get your API key and add to the .ENV file
[https://developer.musixmatch.com](https://developer.musixmatch.com)

## Open API
- Go to `server` directory 
- run `npm run swagger-autogen`
- open `http://localhost:8080/doc`


## Unit Test
- Go to `server` directory 
- run `npm test`

## Docker
- run `docker-compose up -d`


Once everything is installed, you are ready to test it.

Contact: ahmad.alhourani.ite90@gmail.com

